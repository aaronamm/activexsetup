﻿DEMO AND TEST PROJECT

by Suthep Sangvirotjanaphat
No copyright. Free to use and modify.


How to run:
Press F5 or Ctrl+F5 to run. Visual Studio will display 
the Windows Form control inside TestContainer dialog box.


Initial:
Just a feedback to this post on facebook.
https://www.facebook.com/groups/MVCTHAIDEV/permalink/678876095498591/


Reference:
http://www.codeguru.com/csharp/.net/net_general/internet/article.php/c19639/Hosting-NET-Windows-Forms-Controls-in-IE.htm
http://stackoverflow.com/questions/3334664/loading-net-usercontrols-in-ie-with-net-4-0
http://stackoverflow.com/questions/12109642/loading-net-usercontrols-in-ie-with-net-4-5
