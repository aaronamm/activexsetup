﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace GF.DotNetControlInIE {
  public partial class DriveList : UserControl {
    public DriveList() {
      InitializeComponent();
    }

    private void DriveList_Load(object sender, EventArgs e) {
      DriveInfo[] drives = DriveInfo.GetDrives();
      foreach (DriveInfo drv in drives) {
        listBox1.Items.Add(drv.Name);
      }
    }
  }
}
