﻿using System;
using System.Drawing.Printing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.JScript;
using Microsoft.Win32;
using Convert = System.Convert;
using System.Linq;
namespace ActiveXMainProject
{

    [Guid("68BD4E0D-D7BC-4cf6-BEB7-CAB950161E79")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IControlEvent
    {
        //Add a DispIdAttribute to any members in the source interface to specify the COM DispId.
        [DispId(0x60020001)]
        void OnClose(string redirectUrl); //This method will be visible from JS

        [DispId(0x60020002)]
        void OnOpen(string msg);

    }

    /// <summary>
    /// Event handler for events that will be visible from JavaScript
    /// </summary>
    public delegate void ControlEventHandler(string redirectUrl);


    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    [ProgId("ActiveXMainProject")]
    [ClassInterface(ClassInterfaceType.AutoDual),
    ComSourceInterfaces(typeof(IControlEvent))] //Implementing interface that will be visible from JS
    [Guid("5F2B907E-579A-4D03-9366-A638C3C06715")]
    [ComVisible(true)]
    public class ActiveXObject
    {
        private string myParam = "Empty";

        public ActiveXObject()
        {

        }

        public event ControlEventHandler OnClose;

        public event ControlEventHandler OnOpen;
        /// <summary>
        /// Opens application. Called from JS
        /// </summary>
        [ComVisible(true)]
        public void Open()
        {
            //TODO: Replace the try catch in aspx with try catch below. The problem is that js OnClose does not register.
            try
            {
                var openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "Please select one photo to show on web page";
                var openResult = openFileDialog.ShowDialog();

                if (openResult == DialogResult.OK)
                {
                    var imageData = Convert.ToBase64String(
                        File.ReadAllBytes(openFileDialog.FileName));


                    if (OnOpen != null) //check if someone regiser it
                    {
                        OnOpen(imageData);
                    }
                }
                //MessageBox.Show(myParam); //Show param that was passed from JS

                //Close(); //Close application

            }
            catch (Exception e)
            {
                //ExceptionHandling.AppException(e);
                throw e;
            }
        }

        /// <summary>
        /// property visible from JS
        /// </summary>
        [ComVisible(true)]
        public string Name
        {
            get
            {
                return myParam;
            }
            set
            {
                myParam = value;
            }
        }


        private void Close()
        {
            if (OnClose != null)
            {
                OnClose("http://otherwebsite.com"); //Calling event that will be catched in JS
            }
            else
            {
                MessageBox.Show("No Event Attached"); //If no events are attached send message.
            }
        }

        public ArrayObject GetArray()
        {
            var objectArray = new object[] { "m1", "m2", "m3", "m4" };
            return GlobalObject.Array.ConstructArray(objectArray);
        }

        public ArrayObject ListAllPrinters()
        {
            var printerNames = PrinterSettings
                .InstalledPrinters.Cast<object>().ToArray();

            return GlobalObject.Array.ConstructArray(printerNames);

        }
    }

}